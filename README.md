# Тестовое задание CloudShop
Система учёта движения товара

## Особенности
* Swagger документация http://swagger.io/
* JsonAPI формат http://jsonapi.org/

## Зависимости
* Docker

## Установка и запуск:
1 - Клонируем репозиторий:
```
$ git clone https://bitbucket.org/PavelPustota/cloudshop
```

2 - Настраиваем подключение к БД:
```
$ cp .env.example .env
$ nano .env
```

3 - Запускаем сервис:
```
$ docker-compose up -d
```
Первый запуск лучше выполнять без флага `-d`, чтобы по логам можно было увидеть, когда установятся все зависимости через composer.

4 - Открываем в браузере:
```
$ open http://localhost:8080
```

5 - Запуск тестов
```
$ docker-compose exec php "/app/test.sh"
```
