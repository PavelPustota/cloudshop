<?php

namespace App;

use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Invoice
 *
 * @SWG\Definition(
 *   definition="JSONAPI_Invoices",
 *   type="object",
 *   @SWG\Property(
 *     property="data",
 *     description="The JSON API list of invoices.",
 *     type="array",
 *     @SWG\Items(
 *       type="object",
 *       required={"type", "id", "attributes"},
 *       @SWG\Property(property="type", type="string", example="invoice"),
 *       @SWG\Property(property="id", type="string", example="5b37ec07dfaf900006121052"),
 *       @SWG\Property(
 *         property="attributes",
 *         type="object",
 *         allOf={
 *           @SWG\Schema(ref="#/definitions/Invoice")
 *         }
 *       )
 *     )
 *   )
 * ),
 *
 * @SWG\Definition(
 *   definition="JSONAPI_Invoice",
 *   type="object",
 *   @SWG\Property(
 *     property="data",
 *     description="The JSON API Invoice.",
 *     type="object",
 *     required={"type", "id", "attributes"},
 *     @SWG\Property(property="type", type="string", example="invoice"),
 *     @SWG\Property(property="id", type="string", example="5b37ec07dfaf900006121052"),
 *     @SWG\Property(
 *       property="attributes",
 *       type="object",
 *       allOf={
 *         @SWG\Schema(ref="#/definitions/Invoice")
 *       }
 *     )
 *   )
 * ),
 *
 * @SWG\Definition(
 *   definition="Invoice",
 *   type="object",
 *   allOf={
 *     @SWG\Schema(ref="#/definitions/NewInvoice"),
 *     @SWG\Schema(
 *       required={"qty_after", "cost"},
 *       @SWG\Property(property="qty_after", format="float32", type="number"),
 *       @SWG\Property(property="cost", format="float32", type="number"),
 *       @SWG\Property(property="created_at", format="date-time", type="string"),
 *       @SWG\Property(property="updated_at", format="date-time", type="string")
 *     )
 *   }
 * )
 *
 * @SWG\Definition(
 *   definition="NewInvoice",
 *   type="object",
 *   required={"pid", "cid", "doc_type", "price", "qty"},
 *   @SWG\Property(property="pid", format="string", type="string"),
 *   @SWG\Property(property="cid", format="string", type="string"),
 *   @SWG\Property(property="doc_type", format="string", type="string"),
 *   @SWG\Property(property="price", format="float32", type="number"),
 *   @SWG\Property(property="qty", format="float32", type="number")
 * )
 *
 * @property string $_id
 * @property string $pid
 * @property string $cid
 * @property string $doc_type
 * @property float $price
 * @property float $qty
 * @property float $qty_after
 * @property float $cost
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App
 */
class Invoice extends Eloquent {

    const DOC_TYPE_SALES = 'sales';
    const DOC_TYPE_PURCHASES = 'purchases';

    const AVAILABLE_DOC_TYPES = [
        self::DOC_TYPE_SALES,
        self::DOC_TYPE_PURCHASES,
    ];

    protected $connection = 'mongodb';

    protected $collection = 'invoices';

    /**
     * @var array
     */
    protected $fillable = [
        'pid',
        'cid',
        'doc_type',
        'price',
        'qty',
    ];
}
