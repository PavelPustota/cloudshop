<?php

namespace App\Http\Requests;

use App\Invoice;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddInvoice extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pid' => 'required|string',
            'cid' => 'required|string',
            'doc_type' => ['required', Rule::in(Invoice::AVAILABLE_DOC_TYPES)],
            'price' => 'required|numeric',
            'qty' => 'required|numeric',
        ];
    }
}
