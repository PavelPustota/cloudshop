<?php

namespace App\Http\Requests;

use App\Invoice;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateInvoice extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pid' => 'string',
            'cid' => 'string',
            'doc_type' => [Rule::in(Invoice::AVAILABLE_DOC_TYPES)],
            'price' => 'numeric',
            'qty' => 'numeric',
        ];
    }
}
