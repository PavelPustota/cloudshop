<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @SWG\Swagger(
 *   basePath="/api",
 *   @SWG\Info(
 *     title="CloudShop API",
 *     version="1.0.0",
 *     description="See http://jsonapi.org/"
 *   ),
 *
 *   @SWG\Definition(
 *     title="A basic JSON API document.",
 *     definition="JSONAPI_BasicDoc",
 *     description="The document MUST contain at least one of the following top-level members: `data`, `errors`, `meta`. The members `data` and `errors` MUST NOT coexist in the same document. The `Self Link` will open a view displaying all existing orders.",
 *     type="object",
 *     @SWG\Property(
 *       property="data",
 *       description="The JSON API document's primary data.",
 *       type="array",
 *       @SWG\Items(
 *         type="object",
 *         required={"type", "id", "attributes"},
 *         @SWG\Property(property="type", type="string"),
 *         @SWG\Property(property="id", type="string"),
 *         @SWG\Property(property="attributes", type="object"),
 *         @SWG\Property(property="relationships", type="object"),
 *       )
 *     ),
 *     @SWG\Property(
 *       property="errors",
 *       type="array",
 *       description="Array of errors.",
 *       @SWG\Items(ref="#/definitions/ErrorModel")
 *     ),
 *     @SWG\Property(
 *       property="included",
 *       type="object"
 *     ),
 *     @SWG\Property(
 *       property="links",
 *       type="object"
 *     ),
 *     @SWG\Property(
 *       property="meta",
 *       type="object"
 *     )
 *   ),
 *
 *   @SWG\Definition(
 *     title="A representation of a JSON API error.",
 *     definition="ErrorModel",
 *     description="A representation of a JSON API error.",
 *     type="object",
 *     required={"status", "title", "detail"},
 *     @SWG\Property(
 *       property="status",
 *       type="integer",
 *       format="int32"
 *     ),
 *     @SWG\Property(
 *       property="title",
 *       type="string"
 *     ),
 *     @SWG\Property(
 *       property="detail",
 *       type="string"
 *     )
 *   ),
 *   @SWG\Definition(
 *     title="Bad request error",
 *     definition="Error400",
 *     description="Bad Request.",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(ref="#/definitions/ErrorModel")
 *     }
 *   ),
 *   @SWG\Definition(
 *     title="Not found error",
 *     definition="Error404",
 *     description="Resource cannot not be found.",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(ref="#/definitions/ErrorModel")
 *     }
 *   ),
 *   @SWG\Definition(
 *     title="Method not allowed error",
 *     definition="Error405",
 *     description="Method not allowed.",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(ref="#/definitions/ErrorModel")
 *     }
 *   ),
 *   @SWG\Definition(
 *     title="Internal server error",
 *     definition="Error500",
 *     description="Internal server error.",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(ref="#/definitions/ErrorModel")
 *     }
 *   )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
