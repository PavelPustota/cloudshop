<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddInvoice;
use App\Http\Requests\UpdateInvoice;
use App\Invoice;
use App\Transformers\InvoiceTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class InvoiceController extends Controller
{
    /**
     * @SWG\Get(
     *   tags={"invoices"},
     *   path="/invoices",
     *   summary="Get list of invoices",
     *   operationId="getInvoices",
     *   @SWG\Response(
     *     response=200,
     *     description="Get list of invoices",
     *     @SWG\Schema(ref="#/definitions/JSONAPI_Invoices")
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="unexpected error",
     *     @SWG\Schema(ref="#/definitions/ErrorModel")
     *   )
     * )
     *
     * Display a listing of the invoices.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $invoices = Invoice::all();
        $invoices = fractal($invoices, new InvoiceTransformer())
            ->withResourceName('invoice')
            ->toArray();

        return response()->json($invoices);
    }

    /**
     * @SWG\Get(
     *   tags={"invoices"},
     *   path="/invoices/{id}",
     *   summary="Get a single invoice based on the ID supplied",
     *   operationId="getInvoice",
     *   @SWG\Parameter(
     *     description="ID of invoice to get",
     *     format="string",
     *     in="path",
     *     name="id",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="invoice get",
     *     @SWG\Schema(ref="#/definitions/JSONAPI_Invoice")
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="unexpected error",
     *     @SWG\Schema(ref="#/definitions/ErrorModel")
     *   )
     * )
     *
     * Display an one invoice.
     *
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     */
    public function get(Request $request, $id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice = fractal($invoice, new InvoiceTransformer())
            ->withResourceName('invoice')
            ->toArray();

        return response()->json($invoice, 200);
    }

    /**
     * @SWG\POST(
     *   tags={"invoices"},
     *   path="/invoices",
     *   summary="Add new invoice",
     *   operationId="addNewInvoices",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="invoice",
     *     in="body",
     *     description="Invoice to add to the storage",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/NewInvoice")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Add new invoice",
     *     @SWG\Schema(ref="#/definitions/JSONAPI_Invoice")
     *   )
     * )
     *
     * Add a new invoice.
     *
     * @param AddInvoice $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function add(AddInvoice $request)
    {
        // TODO: Позволять добавлять записи "в прошлом"

        $validated = $request->validated();

        $lastInvoice = Invoice::where('pid', '=', $validated['pid'])
            ->where('cid', '=', $validated['cid'])
            ->orderBy('created_at', 'desc')
            ->first();

        $qty_before = $lastInvoice->qty ?? 0;
        $cost_before = $lastInvoice->cost ?? 0;
        $qty_after = $qty_before + $validated['qty'];

        /** @var Invoice $invoice */
        $invoice = new Invoice($validated);
        $invoice->qty_after = $qty_after;
        if ($qty_after > 0) {
            $invoice->cost = ($qty_before * $cost_before + $validated['qty'] * $validated['price']) / $qty_after;
        }

        if (!$invoice->save()) {
            throw new \Exception('Could not save invoice.');
        };

        $result = fractal($invoice, new InvoiceTransformer())
            ->withResourceName('invoice')
            ->toArray();

        return response()->json($result, 200);
    }

    /**
     * @SWG\PATCH(
     *   tags={"invoices"},
     *   path="/invoices/{id}",
     *   summary="Update an invoice",
     *   operationId="updateInvoices",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     description="ID of invoice to update",
     *     format="string",
     *     in="path",
     *     name="id",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="invoice",
     *     in="body",
     *     description="Invoice to update in the storage",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/NewInvoice")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Updated an invoice",
     *     @SWG\Schema(ref="#/definitions/JSONAPI_Invoice")
     *   )
     * )
     *
     * Update an invoice.
     *
     * @param UpdateInvoice $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UpdateInvoice $request, $id)
    {
        // TODO: Пересчитывать значения во всех остальных записях
        $validated = $request->validated();

        $invoice = Invoice::findOrFail($id);
        $invoice->update($validated);
        $invoice = fractal($invoice, new InvoiceTransformer())
            ->withResourceName('invoice')
            ->toArray();

        return response()->json($invoice, 200);
    }

    /**
     * @SWG\Delete(
     *   tags={"invoices"},
     *   path="/invoices/{id}",
     *   summary="Delete a single invoice based on the ID supplied",
     *   operationId="deleteInvoice",
     *   @SWG\Parameter(
     *     description="ID of invoice to delete",
     *     format="string",
     *     in="path",
     *     name="id",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=204,
     *     description="invoice deleted"
     *   )
     * )
     *
     * @param Request $request
     * @param string $id
     * @return int
     */
    public function delete(Request $request, $id)
    {
        // TODO: Пересчитывать значения во всех остальных записях

        $invoice = Invoice::findOrFail($id);
        $invoice->delete();

        return response()->json(null, 204);
    }
}
