<?php

namespace App\Transformers;

use App\Invoice;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class InvoiceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Invoice $invoice
     * @return array
     */
    public function transform(Invoice $invoice)
    {
        return [
            'id'  => (string) $invoice->_id,
            'pid' => (string) $invoice->pid,
            'cid' => (string) $invoice->cid,
            'doc_type' => (string) $invoice->doc_type,
            'price' => (float) $invoice->price,
            'qty' => (float) $invoice->qty,
            'qty_after' => (float) $invoice->qty_after,
            'cost' => (float) $invoice->cost,
            'created_at' => Carbon::parse($invoice->created_at)->setTimezone('UTC')->format(Carbon::ISO8601),
            'updated_at' => Carbon::parse($invoice->updated_at)->setTimezone('UTC')->format(Carbon::ISO8601),
        ];
    }
}
