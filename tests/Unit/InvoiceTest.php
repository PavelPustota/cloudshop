<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvoiceTest extends TestCase
{
    /**
     * Test for check exists class.
     *
     * @return void
     */
    public function testExists()
    {
        $this->assertTrue(class_exists('App\Invoice'), 'Invoice class not found');
        $this->assertTrue(class_exists('App\Http\Requests\AddInvoice'), 'AddInvoice class not found');
        $this->assertTrue(class_exists('App\Http\Requests\UpdateInvoice'), 'UpdateInvoice class not found');
        $this->assertTrue(class_exists('App\Http\Controllers\InvoiceController'), 'InvoiceController class not found');
        $this->assertTrue(class_exists('App\Transformers\InvoiceTransformer'), 'InvoiceTransformer class not found');
    }
}
