<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvoiceTest extends TestCase
{
    /**
     * Test for check get list of invoices endpoints.
     *
     * @return void
     */
    public function testGetList()
    {
        $response = $this->get('/api/invoices');

        $response->assertStatus(200);
    }
}
