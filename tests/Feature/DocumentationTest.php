<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DocumentationTest extends TestCase
{
    /**
     * Test for check exists documentation.
     *
     * @return void
     */
    public function testExists()
    {
        $response = $this->get('/api/documentation');

        $response->assertStatus(200);
    }
}
