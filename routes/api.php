<?php

use Illuminate\Http\Request;

Route::get('invoices', 'InvoiceController@index');
Route::get('invoices/{id}', 'InvoiceController@get');
Route::patch('invoices/{id}', 'InvoiceController@update');
Route::post('invoices', 'InvoiceController@add');
Route::delete('invoices/{id}', 'InvoiceController@delete');
